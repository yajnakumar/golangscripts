package main

import (
	"net/http"
	"./env"
	"io"
	"fmt"
	"net/url"
	"log"
	"io/ioutil"
	"github.com/Jeffail/gabs"
	"github.com/rs/cors"
	"github.com/gorilla/mux"
	"encoding/base64"
)

func getAllByQueryAPI (w http.ResponseWriter, r *http.Request) {
	token := getToken()
	query := &url.URL{Path: env.GET_ALLAPI_BY_QUERY}
	res, err := http.Get(fmt.Sprintf(env.BASE_QUERY_URL, token, query.String()));
	if err != nil {
		panic(err)
	}

	defer res.Body.Close()
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.Copy(w, res.Body)
}

func getMostPopularByQueryAPI (w http.ResponseWriter, r *http.Request) {
	token := getToken()
	query := &url.URL{Path: env.GET_MOSTPOPULAR_BY_QUERY }
	log.Println(query);
	res, err := http.Get(fmt.Sprintf(env.BASE_QUERY_URL, token, query.String()));
	if err != nil {
		panic(err)
	}

	defer res.Body.Close()
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.Copy(w, res.Body)
}

func getMostRecentByQueryAPI (w http.ResponseWriter, r *http.Request) {
	token := getToken()
	query := &url.URL{Path: env.GET_MOSTRECENT_BY_QUERY }
	res, err := http.Get(fmt.Sprintf(env.BASE_QUERY_URL, token, query.String()));
	if err != nil {
		panic(err)
	}

	defer res.Body.Close()
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.Copy(w, res.Body)
}


func getAlphabeticalByQueryAPI (w http.ResponseWriter, r *http.Request) {
	token := getToken()
	query := &url.URL{Path: env.GET_ALPHABETICAL_BY_QUERY }
	res, err := http.Get(fmt.Sprintf(env.BASE_QUERY_URL, token, query.String()));
	if err != nil {
		panic(err)
	}

	defer res.Body.Close()
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.Copy(w, res.Body)
}


func getAppMostRecentByQueryAPI (w http.ResponseWriter, r *http.Request) {
	token := getToken()
	query := &url.URL{Path: env.GET_APPLICATION_MOSTRECENT_BY_QUERY }
	res, err := http.Get(fmt.Sprintf(env.BASE_QUERY_URL, token, query.String()));
	if err != nil {
		panic(err)
	}

	defer res.Body.Close()
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.Copy(w, res.Body)
}

func getAppNameFrmCompanyByQuery (w http.ResponseWriter, r *http.Request) {
	token := getToken()
	params := mux.Vars(r)
	company :=  params["company"]
	query := &url.URL{Path: fmt.Sprintf(env.GET_APPNAMES_FROM_COMPANY_BY_QUERY, company)}
	res, err := http.Get(fmt.Sprintf(env.BASE_QUERY_URL, token, query.String()));
	if err != nil {
		panic(err)
	}

	defer res.Body.Close()
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.Copy(w, res.Body)
}

func getSwaggerFileInfo (w http.ResponseWriter, r *http.Request) {
	token := getToken()
	params := mux.Vars(r)
	company :=  params["uid"]
	res, err := http.Get(fmt.Sprintf(env.GET_SWAGGER_FILE_BY_API, token, company));
	rawResult, _ := ioutil.ReadAll(res.Body)
	rawResultStr := string (rawResult[:])
	decodedString,_ := base64.StdEncoding.DecodeString (rawResultStr)

	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(decodedString)
}

func getSearchApiResponseByApi (w http.ResponseWriter, r *http.Request) {
	token := getToken()
	params := mux.Vars(r)
	data :=  params["data"]
	res, err := http.Get(fmt.Sprintf(env.GET_SEARCH_API_RESPONSE_BY_API, token, data));
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.Copy(w, res.Body)
}

func getTokenAPI (w http.ResponseWriter, r *http.Request) {
	response, err := http.Get(env.TOKEN_URL)

	if err != nil {
		panic(err)
	}

	defer response.Body.Close()
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.Copy(w, response.Body)
}

func getToken() string {
	response, err := http.Get(env.TOKEN_URL)

	if err != nil {
		log.Println(err)
		return ""
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Println(err)
		}

		jsonParsed, err := gabs.ParseJSON(contents)
		value, _ := jsonParsed.Path("token").Data().(string)
		tokenvalue := value

		log.Println("Token returning from API: ", tokenvalue)
		return tokenvalue
	}
	return ""
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc(env.GET_TOKEN, getTokenAPI).Methods("GET")
	router.HandleFunc(env.ALL_QUERY_API, getAllByQueryAPI).Methods("GET")
	router.HandleFunc(env.MOSTPOPULAR_QUERY_API, getMostPopularByQueryAPI).Methods("GET")
	router.HandleFunc(env.MOSTRECENT_QUERY_API, getMostRecentByQueryAPI).Methods("GET")
	router.HandleFunc(env.ALPHABETICAL_QUERY_API, getAlphabeticalByQueryAPI).Methods("GET")
	router.HandleFunc(env.APPLICATION_MOST_RECENT_QUERY_API, getAppMostRecentByQueryAPI).Methods("GET")
	router.HandleFunc(env.APPNAMES_QUERY_API, getAppNameFrmCompanyByQuery).Methods("GET")
	router.HandleFunc(env.GET_SWAGGER_FILE, getSwaggerFileInfo).Methods("GET")
	router.HandleFunc(env.GET_SEARCH_API_RESPONSE, getSearchApiResponseByApi).Methods("GET")

	// cors.Default() setup the middleware with default options being
	// all origins accepted with simple methods (GET, POST). See
	// documentation below for more options.
	corsRouter := cors.Default().Handler(router)
	log.Fatal(http.ListenAndServe(env.PORT, corsRouter))

}

