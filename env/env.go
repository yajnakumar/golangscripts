package env

var PORT = ":8080"

var GET_TOKEN = "/gettoken";
var ALL_QUERY_API = "/allbyquery";
var MOSTPOPULAR_QUERY_API = "/mostpopularbyquery";
var MOSTRECENT_QUERY_API = "/mostrecentbyquery";
var ALPHABETICAL_QUERY_API = "/alphabeticalbyquery";
var APPLICATION_MOST_RECENT_QUERY_API = "/appmostrecentbyquery";
var APPNAMES_QUERY_API = "/appnamesfromcompany/{company}"
var GET_SWAGGER_FILE = "/getswaggerfile/{uid}";
var GET_SEARCH_API_RESPONSE = "/getsearchapiresponse/{data}" ;

var MENU_BASE_DISTINCT_QUERY = "SELECT DISTINCT COMPANY_NAME, LOGO_URL FROM aapi_cloud.VW_API_CARDS where OWNER=1 ";
var MENU_BASE_QUERY = "SELECT COMPANY_NAME, LOGO_URL FROM aapi_cloud.VW_API_CARDS where OWNER=1 ";
var GET_ALLAPI_BY_QUERY = MENU_BASE_DISTINCT_QUERY + " ; ";
var GET_MOSTPOPULAR_BY_QUERY = MENU_BASE_QUERY + " ORDER BY ADD_COUNT DESC;";
var GET_MOSTRECENT_BY_QUERY = MENU_BASE_QUERY + " order by PUBLISH_DATE desc;";
var GET_ALPHABETICAL_BY_QUERY = MENU_BASE_QUERY + " order by COMPANY_NAME, APP_NAME, NAME desc;";
var GET_APPLICATION_MOSTRECENT_BY_QUERY = MENU_BASE_QUERY + " order by APP_NAME desc;";

var GET_APPNAMES_FROM_COMPANY_BY_QUERY = "SELECT * FROM aapi_cloud.VW_API_CARDS where company_name=\"%s\";";

var GET_SWAGGER_FILE_BY_API = "https://devadmin.aapi.io/single?token=%s&uniqueid=%s";

var GET_SEARCH_API_RESPONSE_BY_API = "http://devadmin.aapi.io/searchmenu?skip=0&limit=1&token=%s&data=%s";

var BASE_QUERY_URL = "https://devadmin.aapi.io/gettabledata?token=%s&query=%s";

var TOKEN_URL = "https://devadmin.aapi.io/aapiauthorize?client_id=" +
	"da9b69dcbbecd79ff29683bcb0299640" +
	"&redirect_uri=https://callback&response_type=token&scope=innomindsapis,listapi&state=current";


